package interfaces;

public interface ICore {
    public void initialize();
    public IUIController getUIController();
    public IGameController getGameController();
    public IPluginController getPluginController();
}
