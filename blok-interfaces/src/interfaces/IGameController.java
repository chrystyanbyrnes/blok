package interfaces;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;

public interface IGameController {
    public void initialize(ICore core);
    public void state();
    public void setState(String state);
    public int m_stateStatus();
    public void removeBlock(MouseEvent e, Rectangle m_player);
    public void bodiesUpdated(ArrayList<Rectangle2D> points, Dimension size);
    public void bodiesCreated(ArrayList<Rectangle2D> points, Dimension size);
    public HashMap<Rectangle2D, Rectangle> getM_BODYRECT();
    public ISimulator getSimulator();
}
