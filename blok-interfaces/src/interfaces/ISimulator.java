package interfaces;

import java.awt.geom.Rectangle2D;

public interface ISimulator {
    public void start();
    public void stop();
    public void run();
    public void init();     
    public void removeBody(Rectangle2D point);
    public int getPlayerFlag();
}
