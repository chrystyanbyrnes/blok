package interfaces;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public interface IUIController {
    public void initialize(ICore core);
    public void setPlayerImage(String route);
    public void bodiesUpdated(ArrayList<Rectangle2D> points);
    public void bodiesCreated(ArrayList<Rectangle2D> points);
    public void paintComponent(Graphics g);
    public void createMenuItem(String menu, String item);
    public void setM_PLAYER(Rectangle m_player);
}
