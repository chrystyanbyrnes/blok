/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blokjava;

import interfaces.ICore;
import interfaces.IGameController;
import interfaces.IPluginController;
import interfaces.IUIController;

/**
 *
 * @author victa
 */
public class Core implements ICore{
    @Override
    public void initialize() {
        uiController = new UIController();
        gameController = new GameController();
        pluginController = new PluginController();
  
        uiController.initialize(this);
               gameController.initialize(this); 
               pluginController.initialize();
    }

    @Override
    public IUIController getUIController() {
        return uiController;
    }

    @Override
    public IGameController getGameController() {
        return gameController;
    }

    @Override
    public IPluginController getPluginController() {
        return pluginController;
    }
    
    private IUIController uiController;
    private IGameController gameController;
    private IPluginController pluginController;
}
