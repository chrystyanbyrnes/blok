package blokjava;

import interfaces.ICore;
import interfaces.IGameController;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.contacts.Contact;

public class GameController implements IGameController, Runnable, ContactListener {
    
    private ICore core;  
    public Simulator simulator;
    public enum State {INITIAL, RUNNING, YOUWON, YOULOST};
    private State m_state = State.INITIAL;
    private HashMap<Rectangle2D, Rectangle> m_bodyRect = new HashMap<Rectangle2D, Rectangle>(); //Body no parametro do HashMap no lugar do Rectangle2D
    
    @Override
    public void initialize(ICore core) {
        this.core = core;
        simulator = new Simulator(core);
        core.getUIController().setPlayerImage("images/player" + Math.abs((new Random()).nextInt()%9) + ".png");
        simulator.init();
    }
    
    public Simulator getSimulator() {
        return simulator;
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void beginContact(Contact cntct) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void endContact(Contact cntct) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void preSolve(Contact cntct, Manifold mnfld) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void postSolve(Contact cntct, ContactImpulse ci) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public HashMap<Rectangle2D, Rectangle> getM_BODYRECT() {
        return m_bodyRect;
    }
    
    public void removeBlock(MouseEvent e, Rectangle m_player) {
        Rectangle2D toBeRemoved = null;
        for (Rectangle2D point : m_bodyRect.keySet()) {
            java.awt.Rectangle rect = m_bodyRect.get(point);
            if (rect.contains(e.getPoint()) && core.getGameController().m_stateStatus() == 6 && rect != m_player) {
                getSimulator().removeBody(point);
                toBeRemoved = point;
                break;
            }
        }
        if (toBeRemoved != null)
            m_bodyRect.remove(toBeRemoved);
    }
    
    public void bodiesUpdated(ArrayList<Rectangle2D> points, Dimension size) {    
        for (Rectangle2D point : points) {
            Body body = simulator.createBody((float)point.getX(), (float)point.getY(), (float)point.getWidth(), (float)point.getHeight(), true, 1.0f, 0.3f, 0.5f);
            Vec2 position = new Vec2((float)point.getX(), (float)point.getY());
            if (getSimulator().getPlayerFlag() == 1) //body.getUserData() != null
                // Player
                m_bodyRect.get(body).setLocation(size.width/2-28 + (int) position.x, size.height/2-28 - (int) position.y);
            else
                // Block
                m_bodyRect.get(body).setLocation(size.width/2-14 + (int) position.x, size.height/2-14 - (int) position.y);
        }
    }
    
    public void bodiesCreated(ArrayList<Rectangle2D> points, Dimension size) {
        m_bodyRect.clear();
         for (Rectangle2D point : points) {
            Body body = simulator.createBody((float)point.getX(), (float)point.getY(), (float)point.getWidth(), (float)point.getHeight(), true, 1.0f, 0.3f, 0.5f);
            Vec2 position = new Vec2((float)point.getX(), (float)point.getY());
            Rectangle rectangle = new Rectangle();
            
            if (getSimulator().getPlayerFlag() == 1) //body.getUserData() != null
            {
                // Player
                rectangle.setRect(-28, -28, 56, 56);
                rectangle.setLocation(size.width/2-28 + (int) position.x, size.height/2-28 - (int) position.y);
                core.getUIController().setM_PLAYER(rectangle);
            }
            else
            {
                // Block
                rectangle.setRect(-14, -14, 28, 28);
                rectangle.setLocation(size.width/2-14 + (int) position.x, size.height/2-14 - (int) position.y);
            }
            m_bodyRect.put(point, rectangle);
        }
    }
    
    @Override
    public int m_stateStatus() {
        if(m_state != State.RUNNING)
            return 1;
        if(m_state == State.INITIAL)
            return 2;
        if(m_state == State.YOUWON)
            return 3;
        if(m_state == State.YOULOST)
            return 4;
        if(m_state != State.RUNNING)
            return 5;
        if(m_state == State.RUNNING)
            return 6;
        return 0;
    }
    
    @Override
    public void state() {
        switch(m_state) {
            case INITIAL:
                setState(State.RUNNING.toString());
            case YOUWON:
            case YOULOST:
                setState(State.INITIAL.toString());
        }
    }
    
    public void setState(String state) {
        m_state = State.valueOf(state);
        System.out.println(m_state);
        System.out.println(State.INITIAL);
        switch(m_state) {
            case INITIAL:
                core.getUIController().setPlayerImage("images/player" + Math.abs((new Random()).nextInt()%9) + ".png");
                simulator.init();
                simulator.stop();
                break;
            case RUNNING:
                simulator.start();
                break;
        }
    }
}
